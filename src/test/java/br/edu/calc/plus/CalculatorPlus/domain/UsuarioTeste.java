/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.CalculatorPlus.domain;

import br.edu.calc.plus.domain.Usuario;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Julia
 */
public class UsuarioTeste {
    public UsuarioTeste() {
    }
    
    //***Testa se o login é valido
    @Test
    public void testeLoginValido() {
        //cenário
        Usuario u = new Usuario();
        u.setLogin("teste123");
        boolean esperado = true; 
                
        //execução
        boolean obtido = u.loginValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se o login está com espaço
    @Test
    public void testeLoginComEspaco() {
        //cenário
        Usuario u = new Usuario();
        u.setLogin("teste 123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.loginValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se o login tem muitos caracteres  
    @Test
    public void testeLoginExtenso() {
        //cenário
        Usuario u = new Usuario();
        u.setLogin("testetesteteste12345");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.loginValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se o login tem poucos caracteres 
    @Test
    public void testeLoginCurto() {
        //cenário
        Usuario u = new Usuario();
        u.setLogin("tes");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.loginValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
   
    //***Testa se a senha é valida
    @Test
    public void testeSenhaValida() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("Te$t3#123");
        boolean esperado = true; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se a senha só possui caracteres minusculos
    @Test
    public void testeSenhaCaracteresMinusculos() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("te$t3#123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se a senha só possui caracteres maiusculos
    @Test
    public void testeSenhaCaracteresMaiusculos() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("TE$T3#123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se a senha não tem caracteres especiais
    @Test
    public void testeSenhaSemCaracterEspecial() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("Teste123");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se a senha só tem caracteres especiais
    @Test
    public void testeSenhaComCaracterEspecial() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("$$$$$$$$$$");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se a senha só tem letras
    @Test
    public void testeSenhaComLetras() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("TesteTeste");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se a senha não tem numero
    @Test
    public void testeSenhaSemNumeros() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("Te$te#");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
    
    //***Testa se a senha só tem numero
    @Test
    public void testeSenhaComNumeros() {
        //cenário
        Usuario u = new Usuario();
        u.setSenha("123456789");
        boolean esperado = false; 
                
        //execução
        boolean obtido = u.senhaValida();
        
        //verificacção
        assertEquals(esperado, obtido);
    }
}
