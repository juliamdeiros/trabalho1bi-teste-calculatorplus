/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.CalculatorPlus.domain;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Usuario
 */
public class PartidaTeste {
    

    //***Testa pegar todos os acerros do usuário
    @Test
    public void testeGetAcertos() {   
        //cenario 
        Usuario user = new Usuario(1, "Julia", "julia", "julia@julia", "123456", "JF", LocalDate.now().minusYears(30)); 
        
        Jogo j1 = new Jogo(1, 4, 6, EOperator.soma, 10, 10, 20);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 20);
        Jogo j3 = new Jogo(3, 2, 2, EOperator.soma, 4, 5, 20);
        
        List<Jogo> jlista = new ArrayList<>();
        jlista.add(j1);
        jlista.add(j2);
        jlista.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.now().minusYears(30), 20, 1);
        p1.setUsuario(user);
        p1.setJogoList(jlista);
        
        int Esperado = 2;
        
        //execução
        int Obtido = p1.getAcertos();
        
        //verificação
        assertEquals(Esperado, Obtido);
    }
    
    //***Testa pegar todos os erros do usuário
    @Test
    public void testeGetErros() {
        //cenario 
        Usuario user = new Usuario(1, "Julia", "julia", "julia@julia", "123456", "JF", LocalDate.now().minusYears(30)); 
        
        Jogo j1 = new Jogo(1, 4, 6, EOperator.soma, 10, 10, 20);
        Jogo j2 = new Jogo(2, 8, 2, EOperator.subtracao, 6, 6, 20);
        Jogo j3 = new Jogo(3, 2, 2, EOperator.soma, 4, 5, 20);
        
        List<Jogo> jlista = new ArrayList<>();
        jlista.add(j1);
        jlista.add(j2);
        jlista.add(j3);
        
        Partida p1 = new Partida(1, LocalDateTime.now().minusYears(30), 20, 1);
        p1.setUsuario(user);
        p1.setJogoList(jlista);
        
        int Esperado = 1;
        
        //execução
        int Obtido = p1.getErros();
        
        //verificação
        assertEquals(Esperado, Obtido);
    }
   
    //***Testa pegar todos os acerros do usuário mas com a lista vazia
    @Test
    public void testeGetAcertosComListaJogosVazia() {
        //cenario 
        Usuario user = new Usuario(1, "Julia", "julia", "julia@julia", "123456", "JF", LocalDate.now().minusYears(30)); 
        
        Partida p1 = new Partida(1, LocalDateTime.now().minusYears(30), 20, 1);
        p1.setUsuario(user);
        
        try {
            //Execução
            int Obtido = p1.getAcertos();
            fail("Erro: lista vazia!!!");
        } catch (Exception e) {
            assertTrue(true);
        }
    }
    
    //***Testa pegar todos os erros do usuário mas com a lista vazia
    @Test
    public void testeGetErrosComListaJogosVazia() {
        //cenario 
        Usuario user = new Usuario(1, "Julia", "julia", "julia@julia", "123456", "JF", LocalDate.now().minusYears(30)); 
        
        Partida p1 = new Partida(1, LocalDateTime.now().minusYears(30), 20, 1);
        p1.setUsuario(user);
        
        //execução
        try {
            int Obtido = p1.getErros();
            fail("Erro: lista vazia!!!");
        } catch (Exception e) {
            assertTrue(true);
        }
    }
    
    //***Testa adicionar a bonificacao 
    @Test
    public void testeAddBonificacao() {
        //cenario
        Partida p = new Partida();
        p.setBonificacao(10);
        double esperado = 20;
        
        //execução
        p.addBonus(10);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
    }
   
    //***Testa adicionar a bonificacao passando valor 0
    @Test
    public void testeAddBonificacaoZero() {
        //cenario
        Partida p = new Partida();
        p.setBonificacao(15);
        double esperado = 15;
        
        //execução
        p.addBonus(0);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
    }
    
    //***Testa adicionar a bonificacao passando valor negativo
    @Test
    public void testeAddBonificacaoNegativo() {
        //cenario
        Partida p = new Partida();
        p.setBonificacao(15);
        double esperado = -5;
        
        //execução
        p.addBonus(-20);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
    }
    
    //***Testa remover a bonificacao
    @Test
    public void testeRemoverBonus() {
        //cenario
        Partida p = new Partida();
        p.setBonificacao(15);
        double esperado = 10;
        
        //execução
        p.removeBonus(5);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
    }
    
    //***Testa remover a bonificacao passando valor negativo
    @Test
    public void teseRemoverBonusPassandoValorNegativo() {
        //cenario
        Partida p = new Partida();
        p.setBonificacao(15);
        double esperado = -5;
        
        //execução
        p.removeBonus(20);
        double obtido = p.getBonificacao(); 
        
        //verificação
        assertEquals(esperado, obtido);
    }
}
